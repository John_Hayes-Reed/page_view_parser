# frozen_string_literal: true

require_relative '../page_list_format_strategies/base.rb'

module PageListFormatStrategies
  # A presentation strategy that presents a list of page paths in descending
  # order (highest number of views at the top) with their total view count.
  class WithViewCount < Base
    def call
      presentation_strategy = proc do |page|
        "#{page.path} #{page.view_count} visit#{page.view_count > 1 ? 's' : ''}"
      end

      pages.sort_by { |page| -page.view_count }
           .map { |page| page.to_s(strategy: presentation_strategy) }
    end
  end
end
