# frozen_string_literal: true

require_relative 'page.rb'

# Takes a log file where each line has the format of:
#   PAGE_NAME VISITOR_IP
# eg.
#   /about 123.456.789.123
# and turns it into a list of 'page' objects.
# The target object to turn each line in the log into is
# expected to be able to receive page: and visitor: keyword arguments.
class LogParser
  class MalformedLog < StandardError; end

  def initialize(page_class: Page)
    @page_class = page_class
  end

  # arranging by path in a hash first instead of straight into an Array to avoid
  # an iteration of the whole list so far on each check for existence. Instead,
  # using the quicker lookup by key to then later just extract the values into
  # the list.
  def parse_from_file(file_path:)
    page_views = {}.tap do |pages|
      File.open(file_path).each do |line|
        build_page line, pages
      end
    end
    [].push(*page_views.values)
  end

  def parse_from_string(string:)
    page_views = {}.tap do |pages|
      string.split("\n").each do |line|
        build_page line, pages
      end
    end
    [].push(*page_views.values)
  end

  private

  attr_reader :file_path, :page_class

  def build_page(line, pages)
    path, visitor = line.split(' ')
    raise MalformedLog, 'Log malformed' if
      path.nil? || visitor.nil?

    if pages[path].nil? # The current path hasn't been seen before
      pages[path] = page_class.new(path: path, visitors: [visitor])
    else
      pages[path].visitors << visitor
    end
  end
end
