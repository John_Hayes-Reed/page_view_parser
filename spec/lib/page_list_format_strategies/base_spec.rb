# frozen_string_literal: true

require 'spec_helper'
require 'support/shared_examples_for_pages_format_strategies'
require 'page'
require 'page_list_format_strategies/base'

describe PageListFormatStrategies::Base do
  pages = [
    Page.new(path: 'TEST/PATH')
  ]

  it_behaves_like 'a page list formatting strategy', pages
end
