# frozen_string_literal: true

RSpec.shared_examples 'a page list formatting strategy' do |pages|
  it 'requires a page at initialisation' do
    expect { described_class.new }.to raise_error ArgumentError
    expect(described_class.new(pages: pages)).to be_a described_class
  end

  it 'implements a call method' do
    strategy = described_class.new(pages: pages)

    expect(strategy).to respond_to :call
  end

  it 'outputs an Array as a result of execution' do
    strategy = described_class.new(pages: pages)

    output = strategy.call

    expect(output).to be_a Array
  end
end
