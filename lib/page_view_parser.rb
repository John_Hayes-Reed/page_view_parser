#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative 'log_parser.rb'
require_relative 'page_list_format_strategies/with_view_count.rb'
require_relative 'page_list_format_strategies/with_unique_view_count.rb'

ALLOWED_FORMATS = {
  'view_count' => PageListFormatStrategies::WithViewCount,
  'unique_view_count' => PageListFormatStrategies::WithUniqueViewCount
}.freeze
class UnknownFormat < StandardError; end

def parse_page_views(file_path, output_format = nil)
  if !output_format.nil? && !ALLOWED_FORMATS.keys.include?(output_format)
    raise UnknownFormat, "Format must be one of #{ALLOWED_FORMATS}"
  end

  pages = LogParser.new.parse_from_file(file_path: file_path)
  format_strategy = ALLOWED_FORMATS[output_format]

  format_strategy ? format_strategy.new(pages: pages).call : pages.map(&:to_s)
end

if $PROGRAM_NAME == __FILE__
  begin
    puts parse_page_views(*ARGV)
  rescue UnknownFormat, LogParser::MalformedLog, Errno::ENOENT => e
    puts "ERROR: #{e.message}"
    exit(false)
  end
end
