# frozen_string_literal: true

module PageListFormatStrategies
  # Base class that represents the interface all pages format strategy
  # classes must implement.
  class Base
    def initialize(pages:)
      @pages = pages
    end

    # It is expected that all sub class strategies will override this method and
    # provide it's own formatting algorithm / behaviour. The result however is
    # always an array of the new format the Pages have taken.
    def call
      pages
    end

    private

    attr_reader :pages
  end
end
