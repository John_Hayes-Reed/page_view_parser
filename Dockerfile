FROM ruby:2.7.1
WORKDIR /usr/src/app
COPY Gemfile Gemfile.lock page_view_parser.gemspec ./
RUN bundle install
COPY . .
CMD ["./executable/page_view_parser.rb"]
