# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name = 'page_view_parser'
  s.version = '0.0.1'
  s.date = '2020-06-05'
  s.summary = 'Page View Parser'
  s.description = 'A simple webserver parser that outputs page count information'
  s.authors = ['John Hayes-Reed']
  s.email = 'john.hayes.reed@gmail.com'
  s.files = [
    'lib/log_parser.rb',
    'lib/page.rb',
    'lib/page_list_format_strategies/base.rb',
    'lib/page_list_format_strategies/with_view_count.rb',
    'lib/page_list_format_strategies/with_unique_view_count.rb'
  ]

  s.add_development_dependency 'guard'
  s.add_development_dependency 'guard-rspec', '~> 4.7'
  s.add_development_dependency 'rspec', '~> 3.9'
  s.add_development_dependency 'rubocop', '~> 0.85'
  s.add_development_dependency 'simplecov', '~> 0.18'
end
