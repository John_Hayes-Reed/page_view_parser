# Page View Log Parser
This CLI tool takes a log file - placed in the root directory as 
`webserver.log` - and outputs a list of pages with different possible formats of
information. The default format is a list of the unique pages with visitor IP
addresses. There are two other formats currently provided:

- view_count: A list of pages and their **total** view count.
- unique_view_count: A list of pages and their **unique** view count.

## Requirements
- Docker Compose
- make

## installation

- Clone the repository
- navigate to the repository
- run `make build`

## Usage
First ensure that there is a `webserver.log` file in the root directory of the cli application. Three formats are available, and each have their own make task to run them:

- Default Page & Visitors: `make run-basic`
- Page and Total View Count: `make view-count`
- Page and Unique View Count: `make unique-view-count`

Also see it being used in **[The web app created utilizing this gem that can be found at this repository here (along with various usage options for the web app)](https://gitlab.com/John_Hayes-Reed/page_view_parser_web_app)**

## About the code and choices made
### First Thoughts
At first read of the task it seemed like a Page class with default sorting and presentation behaviour with specialisations / subclasses for each of the different ways of sorting and presenting itself, ie. each specialisation could implement it's own `<=>` method for how it should be sorted and it's own `to_s` method for outputting itself. However after writing down the flow and behaviour of the system I expected, I didn't feel comfortable with this approach as the specialisations in this case aren't being used to encapsulate a new special type of behaviour so much as they are providing a way of manipulating the data based on how the outside world wants to view it. 

### A fly-by thought
I then started to think if Ruby refinements could be of use here, could creating refinements of the Page class for each of the formats and `using` them where needed help, although this quickly fell through as after a bit more thought it falls into the same trap as above, not to mention the nature of `using` refinements meant I could envisage having to make some workarounds / adding complexity to make it work like I might want in the single script.

### A final strategy
Once looking at the lines and boxes on my paper a little more, it looked to me like I wanted a different unique algorithm to format and manipulate the `Page`s for each context I wanted to use it in. This sounded suspiciously similar to the Strategy pattern to me (Decoration also came to mind, however an input>output strategy seemed to fit more than a transparent layer of funcitonality in this case) and so that is the approach I decided to try and move ahead on. After which I took my boxes, lines, and notes from paper into specs outlining the initial expected behaviour of different parts of the system, I started making them go green, refining and increasing the behaviour as I learnt more through building, making them go green, and then refactoring. And the rest is history....

### How would I take this further
I think the approach taken with the separation of concerns and responsibilities from the domain model and strategy logic makes this pretty extensible without having to modify any of the existing code, other than adding your new strategy and adding it to the allowed list of formats. 

With this in mind I can think of a few other strategies I think could be useful, a strategy that takes the IP addresses and uses a geocode search on them to get a breadth of locations (countries / cities) for different pages to look for any interesting trends that could show up. Another would be some kind of whois formatting strategy to see if their are any key identities showing an interest (particularly businesses with static ip addresses for example?) in specific pages / products on offer, and if that provides a marketing opportunity.

Another opportunity to take this further would be to package this up as a gem and use it as part of a web api or simple web app in which users can freely upload text in the same format as the log file, have it processed for them, and put back out onto the page in the relevant format.

## Update
I actually decided to do that second opportunity I mentioned above about a web app and so have extended the behaviour to allow not only parsing of a file, but also from a text string and updated this slightly so it can be pushed as a Gem, and have written [a quick basic web api](https://gitlab.com/John_Hayes-Reed/page_view_parser_web_app) that uses this as a gem.

# Development
Docker Compose is used to build the local environment, this is my preferred method of working for multiple reasons, including not cluttering my local machine and more importantly when in team / onboarding situations, quick onboarding and a reduction of "But it works on my machine" syndrome.

A few make tasks have been provided to run certain tools in development:

- **`make build`** to build the project locally.
- **`make guard`** for live listening of file edits and saves and insant automatic rerun of any related tests upon those edits and saves, makes TDDing a dream. (Please note how an editor saves files changes expected behaviour here, so if live test reruns don't happen, try changing editor or changing the command in the Makefile to use the `-p` polling option.)
- **`make rubocop`** for code style checks, this command actually runs it with the `-a` switch for auto amendment, so what can be automatically fixed will be upon running this. (Which will also automatically kick off test runs on the newly formatted code if you have guard running as above).
- **`make rspec`** to manually run the tests if not using guard.
- **`make document-behaviour`** to run the tests in documentation format mode, so the output is a list of the behaviour for each of the components, driven from the statements in the RSpec files.

## Tests and Style
RSpec output after a `make rspec`:
```
............................

Finished in 0.0603 seconds (files took 0.35157 seconds to load)
28 examples, 0 failures

Coverage report generated for RSpec to /usr/src/app/coverage. 235 / 238 LOC (98.74%) covered.
```

Rubocop output after a `make rubocop`:
```
Inspecting 17 files
.................

17 files inspected, no offenses detected
```

Documented behaviour output after a `make document-behaviour`:
```
LogParser
  from a file
    creates an array of objects from a simple log file
    does not duplicate page objects when multiple visits occur
    raises an error when there is a malformed log file
  from plain text
    creates an array of objects from a text string
    does not duplicate page objects when multiple visits occur
    raises an error when there is a malformed log file

PageListFormatStrategies::Base
  behaves like a page list formatting strategy
    requires a page at initialisation
    implements a call method
    outputs an Array as a result of execution

PageListFormatStrategies::WithUniqueViewCount
  outputs a string representation with the count of unique visits
  behaves like a page list formatting strategy
    requires a page at initialisation
    implements a call method
    outputs an Array as a result of execution

PageListFormatStrategies::WithViewCount
  outputs a list ordered by total views with total view count info
  behaves like a page list formatting strategy
    requires a page at initialisation
    implements a call method
    outputs an Array as a result of execution

Page
  can have more visitors pushed
  exposes the number of views it has had
  exposes the number of unique views it has had
  with no custom strategy passed in
    presents itself as a string containing attributes
  with a custom strategy passed in
    presents itself as per the custom strategy

Page View Parser #parse_page_views
  with a valid file
    should parse and output with no specified format
    should parse and output with a view count format
    should parse and output with a unique view count format
    should raise an error when given an unknown format
  with a malformed file
    should raise a MalformedLog error
  with a missing file
    should raise a no such file error

Finished in 0.0596 seconds (files took 0.35243 seconds to load)
28 examples, 0 failures

Coverage report generated for RSpec to /usr/src/app/coverage. 235 / 238 LOC (98.74%) covered.
```