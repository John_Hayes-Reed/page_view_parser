# frozen_string_literal: true

# Represents a record of a page that can be visited, holding a record of the
# visitors via their ip address
class Page
  attr_reader :path, :visitors

  def initialize(path:, visitors: [])
    @path = path
    @visitors = visitors
  end

  def <<(visitor)
    visitors << visitor
  end

  def view_count
    visitors.count
  end

  def unique_view_count
    visitors.uniq.count
  end

  def to_s(strategy: DEFAULT_PAGE_PRESENTATION_STRATEGY)
    strategy.call(self)
  end

  DEFAULT_PAGE_PRESENTATION_STRATEGY = proc do |page|
    "#{page.path} visited by #{page.visitors.uniq}"
  end
end
