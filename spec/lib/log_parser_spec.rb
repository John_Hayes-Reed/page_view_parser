# frozen_string_literal: true

require 'spec_helper'
require 'log_parser.rb'

describe LogParser do
  let(:page_like_class) do
    Struct.new(:path, :visitors, keyword_init: true)
  end

  context 'from a file' do
    it 'creates an array of objects from a simple log file' do
      # The below log file contains a single visit to /page1, /page2, /page3
      file_path = 'spec/support/three_unique_page_views.log'

      output_list = described_class.new(
        page_class: page_like_class
      ).parse_from_file(file_path: file_path)

      aggregate_failures do
        expect(output_list).to be_a Array
        expect(output_list.first).to be_a page_like_class
      end
    end

    it 'does not duplicate page objects when multiple visits occur' do
      # The below log file contains 4 visits to /page1
      file_path = 'spec/support/multiple_visits_to_same_page.log'

      output_list = described_class.new(
        page_class: page_like_class
      ).parse_from_file(file_path: file_path)
      page_1_pages = output_list.select { |page| page.path == '/page1' }
      aggregate_failures do
        expect(page_1_pages.count).to be 1
        expect(page_1_pages.first.visitors.count).to be 4
      end
    end

    it 'raises an error when there is a malformed log file' do
      file_path = 'spec/support/malformed_log_file.log'
      parser = described_class.new(
        page_class: page_like_class
      )

      expect { parser.parse_from_file(file_path: file_path) }
        .to raise_error(LogParser::MalformedLog)
    end
  end

  context 'from plain text' do
    it 'creates an array of objects from a text string' do
      input = <<~INPUT
        /page1 111.111.111.111
        /page2 222.222.222.222
        /page3 333.333.333.33
      INPUT

      output_list = described_class
                    .new(page_class: page_like_class)
                    .parse_from_string(string: input)

      aggregate_failures do
        expect(output_list).to be_a Array
        expect(output_list.first.path).to eq '/page1'
        expect(output_list.first.visitors.count).to be 1
      end
    end

    it 'does not duplicate page objects when multiple visits occur' do
      input = <<~INPUT
        /page1 111.111.111.111
        /page2 222.222.222.222
        /page3 333.333.333.333
        /page1 444.444.444.444
        /page2 555.555.555.555
        /page1 666.666.666.666
        /page1 444.444.444.444
      INPUT

      output_list = described_class
                    .new(page_class: page_like_class)
                    .parse_from_string(string: input)

      page_1_pages = output_list.select { |page| page.path == '/page1' }

      aggregate_failures do
        expect(page_1_pages.count).to be 1
        expect(page_1_pages.first.visitors.count).to be 4
      end
    end

    it 'raises an error when there is a malformed log file' do
      input = <<~INPUT
        /page1 111.111.111.111
        /page2 222.222.222.222
        /page3 
        /page1 444.444.444.444
        /page2 555.555.555.555
        /page1 666.666.666.666
      INPUT

      parser = described_class.new(
        page_class: page_like_class
      )

      expect { parser.parse_from_string(string: input) }
        .to raise_error(LogParser::MalformedLog)
    end
  end
end
