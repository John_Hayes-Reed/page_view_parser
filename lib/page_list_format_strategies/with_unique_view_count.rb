# frozen_string_literal: true

require_relative '../page_list_format_strategies/base.rb'

module PageListFormatStrategies
  # A presentation strategy that presents a list of pages in descending order
  # by the number of unique views with its unique view count.
  class WithUniqueViewCount < Base
    def call
      presentation_strategy = proc do |page|
        str = ''
        str += "#{page.path} #{page.unique_view_count} "
        str += "unique visit#{page.unique_view_count > 1 ? 's' : ''}"
        str
      end

      pages.sort_by { |page| -page.unique_view_count }
           .map { |page| page.to_s(strategy: presentation_strategy) }
    end
  end
end
