# frozen_string_literal: true

require 'spec_helper'
require 'page'

describe Page do
  it 'can have more visitors pushed' do
    page = described_class.new(path: 'TEST/PATH', visitors: ['111.111.111.111'])
    additional_visitor = '222.222.222.222'

    aggregate_failures do
      expect { page << additional_visitor }
        .to change { page.visitors.count }.by 1
      expect(page.visitors).to include additional_visitor
    end
  end

  it 'exposes the number of views it has had' do
    visitors = ['111.111.111.111', '222.222.222.222', '333.333.333.333']
    page = described_class.new(path: 'TEST/PATH', visitors: visitors)

    expect(page.view_count).to eq 3
  end

  it 'exposes the number of unique views it has had' do
    visitors = ['111.111.111.111', '222.222.222.222', '111.111.111.111']
    page = described_class.new(path: 'TEST/PATH', visitors: visitors)

    expect(page.unique_view_count).to eq 2
  end

  context 'with no custom strategy passed in' do
    it 'presents itself as a string containing attributes' do
      visitors = ['111.111.111.111', '222.222.222.222']
      page = described_class.new(path: 'TEST/PATH', visitors: visitors)

      string_presentation = page.to_s

      aggregate_failures do
        expect(string_presentation).to be_a String
        expect(string_presentation).to include 'TEST/PATH'
        expect(string_presentation).to include '111.111.111.111'
        expect(string_presentation).to include '222.222.222.222'
      end
    end
  end

  context 'with a custom strategy passed in' do
    it 'presents itself as per the custom strategy' do
      custom_strategy = proc { |_page| 'Foobar' }

      visitors = ['111.111.111.111', '222.222.222.222']
      page = described_class.new(path: 'TEST/PATH', visitors: visitors)

      string_presentation = page.to_s(strategy: custom_strategy)

      aggregate_failures do
        expect(string_presentation).to be_a String
        expect(string_presentation).to eq 'Foobar'
      end
    end
  end
end
