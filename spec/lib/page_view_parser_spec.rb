# frozen_string_literal: true

require 'spec_helper'
require 'page_view_parser'

describe 'Page View Parser #parse_page_views' do
  context 'with a valid file' do
    let(:file_path) { 'spec/support/basic_example.log' }

    it 'should parse and output with no specified format' do
      output = parse_page_views(file_path)

      aggregate_failures do
        expect(output).to be_a Array
        expect(output.first).to include '/help_page/1'
        expect(output.first).to include '126.318.035.038'
      end
    end

    it 'should parse and output with a view count format' do
      output = parse_page_views(file_path, 'view_count')

      aggregate_failures do
        expect(output).to be_a Array
        expect(output.first).to include '/help_page/1'
        expect(output.first).to include '2 visits'
      end
    end

    it 'should parse and output with a unique view count format' do
      output = parse_page_views(file_path, 'unique_view_count')

      aggregate_failures do
        expect(output).to be_a Array
        expect(output.first).to include '/help_page/1'
        expect(output.first).to include '2 unique visits'
      end
    end

    it 'should raise an error when given an unknown format' do
      expect { parse_page_views(file_path, 'foo') }
        .to raise_error UnknownFormat
    end
  end

  context 'with a malformed file' do
    it 'should raise a MalformedLog error' do
      expect { parse_page_views('spec/support/malformed_log_file.log') }
        .to raise_error LogParser::MalformedLog
    end
  end

  context 'with a missing file' do
    it 'should raise a no such file error' do
      expect { parse_page_views('none/existent/file.log') }
        .to raise_error Errno::ENOENT
    end
  end
end
