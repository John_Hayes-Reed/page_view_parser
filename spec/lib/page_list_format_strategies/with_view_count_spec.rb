# frozen_string_literal: true

require 'spec_helper'
require 'support/shared_examples_for_pages_format_strategies'

require 'page'
require 'page_list_format_strategies/with_view_count'

describe PageListFormatStrategies::WithViewCount do
  pages = [
    Page.new(path: 'TEST/PATH1', visitors: ['222.222.222.222',
                                            '333.333.333.333']),
    Page.new(path: 'TEST/PATH2', visitors: ['222.222.222.222',
                                            '333.333.333.333',
                                            '111.111.111.111']),
    Page.new(path: 'TEST/PATH3', visitors: ['111.111.111.111']),
    Page.new(path: 'TEST/PATH4', visitors: ['222.222.222.222',
                                            '333.333.333.333',
                                            '111.111.111.111',
                                            '444.444.444.444'])
  ]

  it_behaves_like 'a page list formatting strategy', pages

  it 'outputs a list ordered by total views with total view count info' do
    strategy = described_class.new(pages: pages)
    formatted_list = strategy.call

    aggregate_failures do
      expect(formatted_list.first).to include 'TEST/PATH4'
      expect(formatted_list.first).to include '4 visits'
      expect(formatted_list.last).to include 'TEST/PATH3'
      expect(formatted_list.last).to include '1 visit'
    end
  end
end
