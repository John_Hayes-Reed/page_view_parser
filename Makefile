# COMMANDS FOR DEVELOPMENT

build:
	docker-compose build

documented-behaviour:
	docker-compose run page_view_parser rspec -f doc

guard:
	docker-compose run page_view_parser bundle exec guard

rspec:
	docker-compose run page_view_parser rspec

rubocop:
	docker-compose run page_view_parser rubocop -a

# COMMANDS TO RUN THE PROGRAM

run-basic:
	docker-compose run page_view_parser lib/page_view_parser.rb webserver.log

view-count:
	docker-compose run page_view_parser lib/page_view_parser.rb webserver.log view_count

unique-view-count:
	docker-compose run page_view_parser lib/page_view_parser.rb webserver.log unique_view_count